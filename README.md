# Helpful Javascript Functions

A place to keep helpful JS functions

## Array Manipulation

### Transform 1-d array to 2-d array with n number of rows, each row being the nth item in flat array
```
function arrTransNth(arr, rowArr) {
    nestedArr = [];
    rowArr = [0, 1]; // Array indicating number of rows
    rows = rowArr.length;
    rowArr.forEach(row => {
        var res = [];
        for (var i = row; i < arr.length; i+=rows) {
          res.push(arr[i]);
        }
        nestedArr.push(res)
    })
    return nestedArr
}

```

### Transform 1-d array to 2-d array with n number of items in each row
```
function arrTransRows(arr, n) {
    return while(arr.length) nestedArr.push(arr.splice(0, n));
}
```

### Fill array from 0 to n
```
Array.from({ length: n }, (v, i) => i)
```


## DOM Manipulation

### Observe changes in an element
```
//https://codepen.io/vldvel/pen/Komwyv
var target = document.getElementById("element-id");

const options = {
    attributeOldValue: true,
};
const observer = new MutationObserver(callback);
observer.observe(target, options);

function callback (mutations) {
    mutations.forEach(({oldValue}) => {
      console.log(oldValue)
    })
}

// Note: remember to disconnect observer - observer.disconnect()
```